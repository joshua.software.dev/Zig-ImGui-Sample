const std = @import("std");

const glfw = @import("mach-glfw");
const zgl_helpers = @import("zgl");
const zgl = zgl_helpers.binding;
const zimgui = @import("Zig-ImGui");


pub extern fn ImGui_ImplOpenGL3_Init(glsl_version: ?[*:0]const u8) bool;
pub extern fn ImGui_ImplOpenGL3_Shutdown() void;
pub extern fn ImGui_ImplOpenGL3_NewFrame() void;
pub extern fn ImGui_ImplOpenGL3_RenderDrawData(draw_data: *const anyopaque) void;

pub extern fn ImGui_ImplGlfw_InitForOpenGL(window: *anyopaque, install_callbacks: bool) bool;
pub extern fn ImGui_ImplGlfw_Shutdown() void;
pub extern fn ImGui_ImplGlfw_NewFrame() void;

/// Default GLFW error handling callback
fn errorCallback(error_code: glfw.ErrorCode, description: [:0]const u8) void {
    std.log.err("glfw: {}: {s}\n", .{ error_code, description });
}

fn glGetProcAddress(p: glfw.GLProc, proc: [:0]const u8) ?zgl.FunctionPointer {
    _ = p;
    return glfw.getProcAddress(proc);
}

pub fn main() !void {
    glfw.setErrorCallback(errorCallback);
    if (!glfw.init(.{})) {
        std.log.err("failed to initialize GLFW: {?s}", .{glfw.getErrorString()});
        std.process.exit(1);
    }
    defer glfw.terminate();

    // Create our window
    const window = glfw.Window.create(
        640,
        480,
        "mach-glfw + zig-opengl + Zig-ImGui",
        null,
        null,
        .{},
    ) orelse {
        std.log.err("failed to create GLFW window: {?s}", .{glfw.getErrorString()});
        std.process.exit(1);
    };
    defer window.destroy();

    glfw.makeContextCurrent(window);

    const proc: glfw.GLProc = undefined;
    try zgl.load(proc, glGetProcAddress);

    const im_context = zimgui.CreateContext();
    zimgui.SetCurrentContext(im_context);
    {
        const im_io = zimgui.GetIO();
        im_io.IniFilename = null;
    }

    zimgui.StyleColorsDark();
    _ = ImGui_ImplGlfw_InitForOpenGL(@ptrCast(window.handle), true);
    _ = ImGui_ImplOpenGL3_Init(null);

    var input_int: i32 = 0;

    // Wait for the user to close the window.
    while (!window.shouldClose()) {
        glfw.pollEvents();
        const size = window.getFramebufferSize();

        zgl.clearColor(1, 0, 1, 1);
        zgl.clear(zgl.COLOR_BUFFER_BIT);

        ImGui_ImplOpenGL3_NewFrame();
        var im_io = zimgui.GetIO();
        im_io.DisplaySize = zimgui.Vec2.init(@floatFromInt(size.width), @floatFromInt(size.height));
        zimgui.NewFrame();

        zimgui.SetNextWindowSize(zimgui.Vec2.init(300, 300));
        // approximately center window
        zimgui.SetNextWindowPos(zimgui.Vec2.init(
            @floatFromInt((size.width - 300) / 2),
            @floatFromInt((size.height - 300) / 2),
        ));

        // Your code here
        if (zimgui.Begin("Example"))
        {
            _ = zimgui.Text("Hello world!");
            _ = zimgui.Button("A Button");
            _ = zimgui.InputInt("A Number Input", &input_int);
        }
        zimgui.End();

        zimgui.EndFrame();
        zimgui.Render();
        ImGui_ImplOpenGL3_RenderDrawData(zimgui.GetDrawData());

        window.swapBuffers();
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    zimgui.DestroyContext();
}
